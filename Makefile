.DEFAULT_GOAL := all

main: main.c ParameterParser.c  
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -c main.c


makefileWriter: makefileWriter.c Node.c  makefileWriter.h 
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -c makefileWriter.c


Node: Node.c  Node.h 
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -c Node.c


ParameterParser: ParameterParser.c makefileWriter.c Reader.c  ParameterParser.h 
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -c ParameterParser.c


Reader: Reader.c  Reader.h 
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -c Reader.c


all: main makefileWriter Node ParameterParser Reader 
	gcc -Wall -Werror -Wextra -pedantic -std=c11 -o main main.o ParameterParser.o makefileWriter.o Reader.o Node.o 
clean:
	rm -fr *.o
	rm -fr main
