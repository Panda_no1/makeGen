#ifndef READER_H
#define READER_H

typedef struct{
    char** filenames;
    int length;
} Includes;

Includes* parseListOfIncludes(const char *filename);
void clearIncludes(Includes *includes);

#endif
