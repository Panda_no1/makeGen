#pragma once

#include <stdbool.h>

typedef struct Node {
    char *shortFilename;
    char *longFilename;
    int numberOfChildren;
    struct Node **children;
    int numberOfParents;
    struct Node **parents;
    int numberOfHeaders;
    char **headers;
} Node;

/*
 * Funktion, um einen Node zu Erstellen.
 * Uebergeben werden koennen die Anhaengigkeiten des zu erstellenden Nodes,
 * seine Eltern-Nodes, und
 * seine Extraheader, die er benoetigt, sowie
 * der Dateiname, der die Datei beschreibt, zu der dieser Node zugeordnet wird.
 * Als Minimum muss der Dateiname angegeben werden.
 * Alle anderen Parameter akzeptieren 'NULL' und '0' (oder '0u'), wobei darauf
 * geachtet werden muss, dass fuer eine 'leere Liste' der dazugehoerige
 * Laengenparameter immer '0' (oder '0u') gesetzt wird. Sollte eine Liste mit
 * uebergeben werden, dann muss der dazugehoerige Laengenparameter die Laenge
 * der uebergebenen Liste beschreiben. Liefert einen erstellten und
 * initialisierten Zeiger auf einen Node zurueck.
 */
Node *createNode(const char *filename, Node *children,
                 int numberOfChildren, Node *parents,
                 int numberOfParents, char **headers,
                 int numberOfHeaders);

/*
 * Methode um den Inhalt eines Nodes zu löschen und allen verwendeten
 * Speicherplatz wieder freizugeben.
 */
void clearNode(Node *node);

/*
 * Prozedur, welche eine Kind zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem das Kind
 * hinzugefuegt werden soll, sowie die Adresse des Nodes, welcher als neues
 * Kind gelten soll.
 */
void nodeAddChild(Node *node, Node *child);

/*
 * Prozedur, welche ein weiteres Elternteil zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem das weitere
 * Elternteil hinzugefuegt werden soll, sowie die Adresse des Nodes, welcher als
 * neues Elternteil gelten soll.
 */
void nodeAddParent(Node *node, Node *parent);

/*
 * Prozedur, welche einen Extraheader zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem der Extraheader
 * hinzugefuegt werden soll, sowie Zeichenkette, welche diesen Extraheader
 * beschreibt, also dessen kompletten Dateinamen.
 */
void nodeAddHeader(Node *node, char *header);

/*
 * Funktion, welche den gesamten Teilbaum, beginnend beim uebergebenen Node,
 * durchschaut, und einen Node sucht, welcher die angegebene Zeichenkette als
 * langen oder kurzen Dateinamen besitzt.
 * Uebergeben werden dabei der Node, ab dem die Suche begonnen werden soll,
 * sowie die Zeichenkette, nach der gesucht wird. Zurueckgegeben wird ein Zeiger
 * auf den gefunden Node, oder NULL.
 */
Node *searchTreeForNode(Node *treeNode, const char *filename);

/*
 * Prozedur, welche den gesamten Teilbaum, beginnend beim uebergebenen Node,
 * auf der Konsole ausgibt.
 * Dabei werden nur die langenDateinamen der Knoten ausgegeben.
 * Deren Kinder werden nacheinander zeilenweise darunter aufgelistet.
 * Die Kinder werden dabei eingerueckt.
 */
void printNodeTree(const Node *node, char *prefix);

/*
 * Prozedur, die alle Informationen des uebergebenen Nodes
 * auf der Konsole ausgibt.
 */
void printNode(const Node *node);

/*
 * Funktion, welche einen Dateinamen um den Punkt und die Dateiendung verkürzt.
 * Der verkuertzte Name wird am Ende zurueckgegeben.
 */
char *shortenFilename(const char *filename);

/*
 * Funktion, welche den langen und kurzen Dateinamen eines Nodes mit einem
 * gegebenen C-String vergleicht. Gibt true zurueck, wenn der uebergebene
 * C-String mit min. einem der beiden Dateinamen uebereinstimmt.
 */
bool compareToNodesName(const Node *node, const char *name);
