#include <stdio.h>
#include <stdlib.h>

#include "ParameterParser.h"

int main(int argc, char *argv[]){
    if(argc == 1){
        fprintf(stderr, "No file was given.\n");
        exit(1);
    }
    startParser(argc, argv);
    exit(0);
}
