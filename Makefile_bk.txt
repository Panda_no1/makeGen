CC=gcc
CFLAGS=-I. -g -D DEBUG
DEPS = makefileWriter.h Node.h ParameterParser.h Reader.h
OBJ = main.o makefileWriter.o Node.o ParameterParser.o Reader.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)