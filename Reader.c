#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "Reader.h"

char* readLine( FILE *file ){
    if( file == NULL ){
        fprintf( stderr, "No valid file was given!\n");
        exit(2);
    }
    if(feof(file)){
        return NULL;
    }

    int readChars = 0;
    while( fgetc( file ) != '\n' && !feof( file ) ){
        readChars++;
    }
    if(feof(file)){
        return NULL;
    }

    if( !feof( file ) ){
        readChars++;
    }

    fseek( file, -readChars, SEEK_CUR );
    
    char *line = malloc( sizeof( char ) * ( readChars ) );
    if( line == NULL ){
        fprintf( stderr, "There was not enough space to allocate enough memory for a C-String!\n");
        exit(4);
    }

    fread( line, sizeof( char ) * readChars, 1, file );
    line[readChars-1] = '\0'; 

    return line;
}

char* includeExtract( const char *line){
    if( !line ){
        fprintf( stderr, "Es wurde keine Zeile zum Parsen uebergeben uebergeben!\n" );
        exit(2);
    }
    char *include = strstr(line, "#include");
    if( !include ){
        return NULL;
    }

    char *includedFileBegin = strstr(include, "\"");
    if( !includedFileBegin ){
        return NULL;
    }
    char *includedFileEnd = strstr(includedFileBegin+1, "\"");
    if( !includedFileEnd ){
        return NULL;
    }
        
    int length = includedFileEnd - includedFileBegin;
    char *result = malloc(sizeof(char) * (length+1));
    if( !result ){
        fprintf( stderr, "Not enough memory to create a C-String!\n" );
        exit(4);
    }
    strncpy(result, includedFileBegin+1, length);
    result[length-1] = '\0';

    return result;
}

Includes* parseListOfIncludes(const char *filename){
    if( !filename ){
        fprintf( stderr, "Es wurde kein Dateiname uebergeben!\n" );
        exit(2);
    }
    
    FILE *file = fopen(filename, "r");
    if(!file){
        fprintf( stderr, "Die Datei unter '%s' konnte nicht geoeffnet werden!\n", filename );
        exit(5);
    }
    
    char **includes = NULL;
     int length = 0;
    char *line;
    while(!feof(file)){
        line = readLine( file );
        if(!line){
            break;
        }
        char *includeFile = includeExtract(line);
        if(includeFile){
            includes = realloc( includes, sizeof(char*) *(length +1));
            if(!includes){
                fprintf( stderr, "Not enough memory to create a C-String!\n" );
                exit(4);
            }
            includes[length] = includeFile;
            length++;
        }
        if(line){
            free(line);
        }
    }

    Includes *result = malloc(sizeof(Includes));
    if(!result){
        fprintf( stderr, "Not enough memory to create a list of included filenames!\n" );
        exit(4);
    }
    result->filenames = includes;
    result->length = length;
    fclose(file);

    return result;
}

void clearIncludes(Includes *includes){
    if( !includes ){
        fprintf( stderr, "Es wurde kein Zeiger auf ein Includes struct uebergeben!\n" );
        exit(2);
    }
    if(includes->filenames && includes->length == 0){
        fprintf(stderr, "The list of included filenames can't be freed because their number is no longer known!\n");
        exit(8);
    }
    if(!(includes->filenames)){
        for( int i =0; i < includes->length; i++){
            free(includes->filenames[i]);
        }
        free(includes->filenames);
        includes->length = 0;
    }
}
