#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ParameterParser.h"
#include "makefileWriter.h"
#include "Reader.h"
void startParser( int argc, char *argv[] ){
    char *header[argc-1];
    char *source[argc-1];
    Node *nodes[argc-1];
    Includes* in[argc-1];
    int i,n, m;
    int k = 0;
    int j = 0;
    char tmp = 0;

    for(i = 1; i < argc; i++){
        if(argv[i][strlen(argv[i])-1] == 'h')
            header[k++] = argv[i];

        else if(argv[i][strlen(argv[i])-1] == 'c'){
            source[j++] = argv[i];
            nodes[j-1] = createNode(source[j-1],NULL , 0, NULL, 0, NULL, 0);
        }
    }
    for(i = 0; i < j; i++){
        in[i] = parseListOfIncludes(nodes[i]->longFilename);
        for(n = 0; n < in[i]->length; n++){
            if(compareToNodesName(nodes[i], in[i]->filenames[n])){
                for(m = 0; m < k; m++){
                    if(compareToNodesName( nodes[i], in[i]->filenames[n])){
                        tmp = 1;
                        break;
                    }
                }
                if(!tmp){
                    fprintf(stderr, "Header file not found: %s\n", nodes[i]->shortFilename);
                    exit(1);
                }
                tmp = 0;
                nodeAddHeader(nodes[i], in[i]->filenames[n]);
                #ifdef DEBUG
                    printf("newHeader: %s\n", in[i]->filenames[n]);
                #endif
            }
            else{
                for(m = 0; m < j; m++){
                    if(compareToNodesName(nodes[m], in[i]->filenames[n])){
                        nodeAddChild(nodes[i], nodes[m]);
                        nodeAddParent(nodes[m], nodes[i]);
                        tmp = 1;
                        #ifdef DEBUG
                        printNode(nodes[i]);
                        printNode(nodes[m]);
                        printf("%s\n", in[i]->filenames[n]);
                        #endif
                        break;
                    }
                }
                if(!tmp){
                    for(m = 0; m < k; m++){
                        if(strcmp(header[m], in[i]->filenames[n])){
                            nodeAddHeader(nodes[i], in[i]->filenames[n]);
                            tmp = 1;
                            break;
                        }
                    }
                    if(!tmp){
                        fprintf(stderr, "Header file not found: %s\n",  nodes[i]->shortFilename);
                        exit(1);
                    }
                }
                tmp = 0;
            }
        }
    }
    for(i = 0; i < j; i++){
        if(nodes[i]->children == NULL && nodes[i]->parents == NULL){
            fprintf(stderr, "Node with no dependencies was found: %s\n", nodes[i]->longFilename);
            #ifdef DEBUG
                printNode(nodes[i]);
            #endif
            exit(1);
        }
    }
    #ifdef DEBUG
        printf("Write to file!\n");
    #endif
    exportMakefile(nodes, j, "Makefile");

}
