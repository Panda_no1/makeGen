#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "Node.h"

#define WRONG_PARAMETER_RANGE 1
#define NULL_POINTER 2
#define WRONG_STRING_LENGTH 3
#define NOT_ENOUGH_MEMORY 4
#define FILE_NOT_OPEN 5
#define UNKNOWN_INCLUDE 6
#define NO_STARTING_POINT 7
#define CANT_FREE_POINTER 8

/*
 * Methode um den Inhalt eines Nodes zu löschen und allen verwendeten Speicherplatz 
 * wieder freizugeben.
 */
void clearNode(Node *node){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'clearNode' uebergeben (Null-Pointer) (Prozedur 'clearNode')!\n" );
        exit( NULL_POINTER );
    }
    free(node->shortFilename);
    free(node->longFilename);
    if(node->children && node->numberOfChildren != 0 ){
        for( int i = 0; i < node->numberOfChildren; i++){
            Node *child =node->children[i];
            clearNode(child);
        }
        free(node->children);
        node->numberOfChildren = 0;
    } else if(node->children && node->numberOfChildren == 0){
        fprintf(stderr, "Die Liste der Kinder kann nicht geloescht werden, weil die Anzahl der Elemente in der Liste nicht mehr bekannt ist (Prozedur 'clearNode')!\n");
        exit(CANT_FREE_POINTER);
    }
    if(node->parents && node->numberOfParents != 0 ){
        for( int i = 0; i < node->numberOfParents; i++){
            Node *parent =node->parents[i];
            clearNode(parent);
        }
        free(node->parents);
        node->numberOfParents = 0;
    } else if(node->parents && node->numberOfParents == 0){
        fprintf(stderr, "Die Liste der Eltern-Nodes kann nicht geloescht werden, weil die Anzahl der Elemente in der Liste nicht mehr bekannt ist (Prozedur 'clearNode')!\n");
        exit(CANT_FREE_POINTER);
    }
    if(node->headers && node->numberOfHeaders != 0 ){
        for( int i = 0; i < node->numberOfHeaders; i++){
            char *header =node->headers[i];
            free(header);
        }
        free(node->headers);
        node->numberOfHeaders = 0;
    } else if(node->headers && node->numberOfHeaders == 0){
        fprintf(stderr, "Die Liste der Extraheader kann nicht geloescht werden, weil die Anzahl der Elemente in der Liste nicht mehr bekannt ist (Prozedur 'clearNode')!\n");
        exit(CANT_FREE_POINTER);
    }
}

/*
 * Interne Funktion, welche einen Dateinamen um den Punkt und die Dateiendung verkürzt.
 * Der verkuertzte Name wird am Ende zurueckgegeben.
 */
char* shortenFilename(const char *filename){
    int length = strlen(filename);
    char *shortFilename = malloc( (length -1) * sizeof(char));
    if( !shortFilename ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um den langen Dateinamen zu kopieren (Funktion 'shortenFilename')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    strncpy(shortFilename, filename, length-2);
    shortFilename[length-2] = '\0';

    return shortFilename;
}

/* 
 * Interne Funktion, welche den langen und kurzen Dateinamen eines Nodes mit einem gegebenen
 * C-String vergleicht.
 * Gibt true zurueck, wenn der uebergebene C-String mit min. einem der beiden Dateinamen uebereinstimmt.
 */
bool compareToNodesName(const Node *node, const char *name){
    return strcmp(name, node->longFilename) == 0 || strstr(name, node->shortFilename) != NULL;
}

/*
 * Funktion, um einen Node zu Erstellen.
 * Uebergeben werden koennen die Anhaengigkeiten des zu erstellenden Nodes, 
 * seine Eltern-Nodes, und
 * seine Extraheader, die er benoetigt, sowie
 * der Dateiname, der die Datei beschreibt, zu der dieser Node zugeordnet wird.
 * Als Minimum muss der Dateiname angegeben werden.
 * Alle anderen Parameter akzeptieren 'NULL' und '0' (oder '0'), wobei darauf 
 * geachtet werden muss, dass fuer eine 'leere Liste' der dazugehoerige Laengenparameter
 * immer '0' (oder '0') gesetzt wird.
 * Sollte eine Liste mit uebergeben werden, dann muss der dazugehoerige Laengenparameter
 * die Laenge der uebergebenen Liste beschreiben.
 * Liefert einen erstellten und initialisierten Zeiger auf einen Node zurueck.
 */
Node* createNode( const char *filename, Node *children, int numberOfChildren, Node *parents,int numberOfParents, char **headers, int numberOfHeaders ){
    Node *node = malloc( sizeof(Node) );
    if( !node ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um einen 'Node' zu erzeugen (Funktion 'createNode')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    int length = strlen(filename);
    node->longFilename = malloc( (length +1) * sizeof(char));
    if( !node->longFilename ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um den langen Dateinamen zu kopieren (Funktion 'createNode')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    strcpy( node->longFilename, filename);
    node->longFilename[length]= '\0';
    node->shortFilename = shortenFilename(filename);

    if( children && numberOfChildren != 0 ){
        Node **childs = malloc( numberOfChildren * sizeof(Node*));
        if( !childs ){
            fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Kinder zu kopieren (Funktion 'createNode')!\n" );
            exit( NOT_ENOUGH_MEMORY );
        }
        for(int i = 0; i < numberOfChildren; i++){
            childs[i] = children+i;
        }

        node->children = childs;
        node->numberOfChildren = numberOfChildren;
    }else if(!children && numberOfChildren != 0){
            fprintf( stderr, "Es wurde keine Adresse fuer die Liste der Kinder gegeben, aber die Laenge der Liste soll nicht 0 sein (Funktion 'createNode')!\n" );
        exit(WRONG_PARAMETER_RANGE);
    }else{
        node->children = NULL;
        node->numberOfChildren = 0;
    }
    if( parents && numberOfParents != 0 ){
        Node **pars = malloc( numberOfParents * sizeof(Node*));
        if( !pars ){
            fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Eltern-Nodes zu kopieren (Funktion 'createNode')!\n" );
            exit( NOT_ENOUGH_MEMORY );
        }
        for(int i = 0; i < numberOfParents; i++){
            pars[i] = parents+i;
        }

        node->parents = pars;
        node->numberOfParents = numberOfParents;
    }else if(!parents && numberOfParents != 0){
        fprintf( stderr, "Es wurde keine Adresse fuer die Liste der Eltern-Nodes gegeben, aber die Laenge der Liste soll nicht 0 sein (Funktion 'createNode')!\n" );
        exit(WRONG_PARAMETER_RANGE);
    }else{
        node->parents = NULL;
        node->numberOfParents = 0;
    }
    if( headers && numberOfHeaders != 0 ){
        char **heads = malloc( numberOfHeaders * sizeof(char*));
        if( !heads ){
            fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Extraheader zu kopieren (Funktion 'createNode')!\n" );
            exit( NOT_ENOUGH_MEMORY );
        }
        memcpy( heads, headers, numberOfHeaders*sizeof(char*));

        node->headers = heads;
        node->numberOfHeaders = numberOfHeaders;
    }else if(!headers && numberOfHeaders != 0){
        fprintf( stderr, "Es wurde keine Adresse fuer die Liste der Extraheader gegeben, aber die Laenge der Liste soll nicht 0 sein (Funktion 'createNode')!\n" );
        exit(WRONG_PARAMETER_RANGE);
    }else{
        node->headers = NULL;
        node->numberOfHeaders = 0;
    }

    return node;
}

/*
 * Prozedur, welche eine Kind zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem das Kind
 * hinzugefuegt werden soll, sowie die Adresse des Nodes, welcher als neues
 * Kind gelten soll.
 */
void nodeAddChild( Node *node, Node *child ){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'nodeAddChild' uebergeben (Null-Pointer) (Prozedur 'nodeAddChild')!\n" );
        exit( NULL_POINTER );
    }
    if( !child){
        fprintf( stderr, "Es wurde kein Kind-Node an die Prozedur 'nodeAddChild' uebergeben (Null-Pointer) (Prozedur 'nodeAddChild')!\n" );
        exit( NULL_POINTER );
    }
    Node **newChilds = malloc(sizeof(Node*) *(node->numberOfChildren +1u));
    if( !newChilds ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Liste der Kinder zu erweitern (Prozedur 'nodeAddChild')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    memcpy(newChilds, node->children, sizeof(Node*)*node->numberOfChildren);
    free(node->children);
    node->children = newChilds;
    node->children[node->numberOfChildren] = child;

    node->numberOfChildren++;
}

/*
 * Prozedur, welche ein weiteres Elternteil zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem das weitere Elternteil
 * hinzugefuegt werden soll, sowie die Adresse des Nodes, welcher als neues
 * Elternteil gelten soll.
 */
void nodeAddParent( Node *node, Node *parent ){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'nodeAddParent' uebergeben (Null-Pointer) (Prozedur 'nodeAddParent')!\n" );
        exit( NULL_POINTER );
    }
    if( !parent){
        fprintf( stderr, "Es wurde kein Eltern-Node an die Prozedur 'nodeAddParent' uebergeben (Null-Pointer) (Prozedur 'nodeAddParent')!\n" );
        exit( NULL_POINTER );
    }
    Node **newPars = malloc(sizeof(Node*) *(node->numberOfParents +1u));
    if( !newPars ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Liste der Eltern-Nodes zu erweitern (Prozedur 'nodeAddParent')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    memcpy(newPars, node->parents, sizeof(Node*)*node->numberOfParents);
    free(node->parents);
    node->parents = newPars;
    node->parents[node->numberOfParents] = parent;

    node->numberOfParents++;
}

/*
 * Prozedur, welche einen Extraheader zu einem Node hinzufuegt.
 * Uebergeben werden dabei die Adresse des Nodes, zu welchem der Extraheader
 * hinzugefuegt werden soll, sowie Zeichenkette, welche diesen Extraheader
 * beschreibt, also dessen kompletten Dateinamen.
 */
void nodeAddHeader( Node *node, char *header ){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'nodeAddHeader' uebergeben (Null-Pointer) (Prozedur 'nodeAddHeader')!\n" );
        exit( NULL_POINTER );
    }
    if( !header){
        fprintf( stderr, "Es wurde keine Zeichenkette fuer den Dateinamen des Extraheaders an die Prozedur 'nodeAddHeader' uebergeben (Null-Pointer) (Prozedur 'nodeAddHeader')!\n" );
        exit( NULL_POINTER );
    }
    char **newHeads = malloc(sizeof(char*) *(node->numberOfHeaders +1u));
    if( !newHeads ){
        fprintf( stderr, "Nicht genuegend Speicherplatz vorhanden, um die Liste der Extraheader zu erweitern (Prozedur 'nodeAddHeader')!\n" );
        exit( NOT_ENOUGH_MEMORY );
    }
    memcpy(newHeads, node->headers, sizeof(char*)*node->numberOfHeaders);
    node->headers = newHeads;
    node->headers[node->numberOfHeaders] = header;

    node->numberOfHeaders++;
}

/*
 * Funktion, welche den gesamten Teilbaum von Nodes, beginnend beim uebergebenen Node, 
 * durchschaut, und einen Node sucht, welcher die angegebene Zeichenkette als
 * langen oder kurzen Dateinamen besitzt.
 * Uebergeben werden dabei der Node, ab dem die Suche begonnen werden soll, sowie
 * die Zeichenkette, nach der gesucht wird.
 * Zurueckgegeben wird ein Zeiger auf den gefunden Node, oder NULL.
 */
Node* searchTreeForNode( Node *treeNode, const char *filename){
    if( !treeNode ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'searchTreeForNode' uebergeben (Null-Pointer) (Prozedur 'searchTreeForNode')!\n" );
        return NULL;
    }
    if( !filename ){
        fprintf( stderr, "Es wurde keine Zeichenkette fuer den zu suchenden Dateinamen an die Prozedur 'searchTreeForNode' uebergeben (Null-Pointer) (Prozedur 'searchTreeForNode')!\n" );
        return NULL;
    }
    if(compareToNodesName(treeNode, filename)){
        return treeNode;
    } else {
        Node *result = NULL;
        for( int i = 0; i < treeNode->numberOfChildren; i++){
            result = searchTreeForNode(treeNode->children[i], filename);
            if(result){
                return result;
            }
        }
    }
    return NULL;
}

/*
 * Prozedur, welche den gesamten Teilbaum, beginnend beim uebergebenen Node, 
 * auf der Konsole ausgibt.
 * Dabei werden nur die langenDateinamen der Knoten ausgegeben.
 * Deren Kinder werden nacheinander zeilenweise darunter aufgelistet.
 * Die Kinder werden dabei eingerueckt.
 */
void printNodeTree(const Node *node, char* prefix){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'printNodeTree' uebergeben (Null-Pointer) (Prozedur 'printNodeTree')!\n" );
        fprintf( stderr, "No node was given!\n" );
        exit(NULL_POINTER);
    }
    printf( "%sNode: \"%s\"\n",prefix,node->longFilename);
    if(node->numberOfChildren > 0 && node->children){
        int length = strlen(prefix);
        char* newPrefix = malloc(sizeof(char) * (length+2));
        strcpy(newPrefix, prefix);
        newPrefix[length] = '\t';
        newPrefix[length+1] = '\0';
        for( int i = 0; i < node->numberOfChildren; i++){
            printNodeTree(node->children[i], newPrefix);
        }
        if(newPrefix){
            free(newPrefix);
        }
    } 
}

/*
 * Prozedur, alle Informationen des uebergebenen Nodes
 * auf der Konsole ausgibt.
 */
void printNode(const Node *node){
    if( !node ){
        fprintf( stderr, "Es wurde kein Node an die Prozedur 'printNode' uebergeben (Null-Pointer) (Prozedur 'printNode')!\n" );
        exit(NULL_POINTER);
    }
    printf("Node: name: '%s' ('%s'), #children: %d, ", node->longFilename,node->shortFilename, node->numberOfChildren);
    if(node->numberOfChildren > 0 && node->children){
        printf("children: ");
        for( int i = 0; i < node->numberOfChildren; i++){
            printf("'%s', ", node->children[i]->longFilename);
        }
    }
    printf("#parents: %d, ", node->numberOfParents);
    if(node->numberOfParents > 0 && node->parents){
        printf("parents: ");
        for( int i = 0; i < node->numberOfParents; i++){
            printf("'%s', ", node->parents[i]->longFilename);
        }
    }
    printf("#headers: %d, ", node->numberOfHeaders);
    if(node->numberOfHeaders > 0 && node->headers){
        printf("headers: ");
        for( int i = 0; i < node->numberOfHeaders; i++){
            printf("'%s', ", node->headers[i]);
        }
    }
    printf("\n");
}
