#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "makefileWriter.h"
#include "Node.h"

void exportMakefile(Node **nodes,int numberOfNodes, const char *makefileName){
    int i, j,n , m, tmp;
    int countCurrent, countShortO, countNext;
    char strDp[512]="";
    char strHead[512]="";
    char strShort [512]="";
    char strO[512]="";
    char *strShortO[128];
    FILE *file;
    Node *currentNodes[numberOfNodes];
    Node *nextNodes[numberOfNodes];
    file = fopen(makefileName, "w");

    fprintf(file, ".DEFAULT_GOAL := all\n\n");
    for(i = 0; i < numberOfNodes; i++){
        for(j = 0; j < nodes[i]->numberOfChildren; j++){
            strcat(strcat(strDp, nodes[i]->children[j]->longFilename)," ");
        }
        for(m = 0; m < nodes[i]->numberOfHeaders; m++){
            strcat(strcat(strHead, nodes[i]->headers[m])," ");
        }
        fprintf(file, "%s: %s %s %s\n", nodes[i]->shortFilename, nodes[i]->longFilename, strDp, strHead);
        fprintf(file, "\t%s %s -c %s\n\n\n", COMPILER, FLAGS, nodes[i]->longFilename);
        strcat(strcat(strShort,nodes[i]->shortFilename), " ");
        strcpy(strDp, "");
        strcpy(strHead, "");
    }

    //root nodes
    countCurrent = 0;
    countNext = 0;
    countShortO = 0;
    for(i = 0; i < numberOfNodes; i++) {
        #ifdef DEBUG
            printf("node: %s has %d children\n", nodes[i]->longFilename, nodes[i]->numberOfChildren);
        #endif
        if(nodes[i]->numberOfParents == 0)

                

            currentNodes[countCurrent++] = nodes[i];
            #ifdef DEBUG
                printf("added: %s to current nodes\n", nodes[i]->longFilename);
            #endif
           
    }

    //loop through current level nodes
    while(countCurrent != 0) {
        for(i = 0; i < countCurrent; i++) {
            //add current node to short list
            strShortO[countShortO++] = currentNodes[i]->shortFilename;
            //loop through children of current node
            for(j = 0; j < currentNodes[i]->numberOfChildren; j++) {
                //loop through already added files
                tmp = 0;
                for(n = 0; n < countShortO; n++) {
                    #ifdef DEBUG
                        printf("compare: %s to %s\n", currentNodes[i]->children[j]->longFilename, strShortO[n]);
                    #endif
                    if(compareToNodesName(currentNodes[i]->children[j], strShortO[n])) {
                        
                        tmp = 1;
                        break;
                    }
                }
                if(!tmp) {
                    nextNodes[countNext++] = currentNodes[i]->children[j];
                    #ifdef DEBUG
                        printf("added: %s to next nodes\n", currentNodes[i]->children[j]->longFilename);
                    #endif
                }

            }
        }
        memcpy(currentNodes, nextNodes, sizeof(Node) * countNext);
        countCurrent = countNext;
        countNext = 0;    
    }

    for(i = 0; i < countShortO; i++) {
        strcat(strcat(strO,strShortO[i]), ".o ");
    }


    fprintf(file, "all: %s\n", strShort);
    fprintf(file, "\t%s %s -o main %s\n", COMPILER, FLAGS, strO);
    fprintf(file, "clean:\n");
    fprintf(file, "\trm -fr *.o\n");
    fprintf(file, "\trm -fr main\n");

    fclose(file);

    // Erweitern Sie die Prozedur, um die entsprechende Funktionalitaet.
}

