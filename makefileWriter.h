#ifndef MAKEFILEWRITER_H
#define MAKEFILEWRITER_H
#define COMPILER "gcc"
#define FLAGS "-Wall -Werror -Wextra -pedantic -std=c11"
#include "Node.h"

void exportMakefile(Node **nodes, int numberOfNodes, const char *makefileName);

#endif
